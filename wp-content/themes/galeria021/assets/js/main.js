/*
 * PLACEHOLDER
 */

$(document).ready(function() {

  if (!Modernizr.input.placeholder) {
    $('[placeholder]').focus(function() {
      var input = $(this);
      if (input.val() == input.attr('placeholder')) {
        input.val('');
        input.removeClass('placeholder');
      }
    }).blur(function() {
      var input = $(this);
      if (input.val() == '' || input.val() == input.attr('placeholder')) {
        input.addClass('placeholder');
        input.val(input.attr('placeholder'));
      }
    }).blur();
    $('[placeholder]').parents('form').submit(function() {
      $(this).find('[placeholder]').each(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
        }
      })
    });
  }
});

function ativador() {
  $('*[title]').tooltip({
    'animation': true,
    'placement': 'bottom',
    'selector': false,
    'trigger': 'hover',
    'destroy': true
  });
}
//ativadores do bootstrap
$(document).ready(function() {
  $('.dropdown-toggle').dropdown();

  $('#carousel').carousel({
    interval: 5000,
    pause: 'hover'
  });

  $('#banners').carousel({
    interval: 500000,
    pause: 'hover'
  });

  $('.dropdown-toggle').on('click', function() {
    if ($('.dropdown').hasClass('opening') == true) {
      $('.dropdown').removeClass('opening').removeClass('open');
    } else {
      $(this).parents('.dropdown').addClass('opening').addClass('open');
    }
  });

  ativador();

  $('.outros-produtos .row').cycle({
    fx: "scrollHorz",
    carouselVisible: 1,
    speed: "600",
    pager: '.pagers',
    prev: ".prev",
    next: ".next",
    slides: '.item',
    timeout: 0,
    allowWrap: false,
    swipe: true
  });

  $('.lightbox-prod').click(function(e) {
    e.preventDefault();
    var $PREVIEW = $(this).data('preview');
    var $HREF = $(this).attr('href');
    var $MINI = $(this).find('img').attr('src');

    var $PREVIEW_PRINC = $('#img-prod img').attr('src');
    var $HREF_PRINC = $('#img-prod').attr('href');
    var $MINI_PRINC = $('#img-prod').data('mini');
    
    $(this).parents('figure').find('.prod-lightbox').attr('href', $HREF_PRINC);

    $('#img-prod').attr('href', $HREF);
    $('#img-prod img').attr('src', $PREVIEW);
    $('#img-prod').data('mini', $MINI);

    $(this).data('preview', $PREVIEW_PRINC);
    $(this).attr('href', $HREF_PRINC);
    $(this).find('img').attr('src', $MINI_PRINC);

  })

});

//paralax
$('#index_bg').parallax({
  'elements': [
    {
      'selector': '#index_bg',
      'properties': {
        'x': {
          'background-position-x': {
            'initial': -5,
            'multiplier': 0.2,
            'invert': false
          }
        }
      }

    },
    {
      'selector': 'div.inner',
      'properties': {
        'x': {
          'background-position-x': {
            'initial': 0,
            'multiplier': 0.2
          }
        }
      }
    }
  ]

});

$('#parallax_left').parallax({
  'elements': [
    {
      'selector': '#parallax_left',
      'properties': {
        'x': {
          'background-position-x': {
            'initial': 7,
            'multiplier': 0.2,
            'invert': false
          }
        }
      }

    },
    {
      'selector': 'div.inner',
      'properties': {
        'x': {
          'background-position-x': {
            'initial': 1500,
            'multiplier': 0.2
          }
        }
      }
    }
  ]

});

$('#parallax_right').parallax({
  'elements': [
    {
      'selector': '#parallax_right',
      'properties': {
        'x': {
          'background-position-x': {
            'initial': 29,
            'multiplier': 0.2,
            'invert': true
          }
        }
      }

    },
    {
      'selector': 'div.inner',
      'properties': {
        'x': {
          'background-position-x': {
            'initial': 0,
            'multiplier': 0.2
          }
        }
      }
    }
  ]

});

$('.header_line_l').parallax({
  'elements': [
    {
      'selector': '.header_line_l',
      'properties': {
        'x': {
          'background-position-x': {
            'initial': -178,
            'multiplier': 0.2,
            'invert': false
          }
        }
      }

    },
    {
      'selector': 'div.inner',
      'properties': {
        'x': {
          'background-position-x': {
            'initial': 0,
            'multiplier': 0.2
          }
        }
      }
    }
  ]

});

$('.header_line_r').parallax({
  'elements': [
    {
      'selector': '.header_line_r',
      'properties': {
        'x': {
          'background-position-x': {
            'initial': -218,
            'multiplier': 0.2,
            'invert': true
          }
        }
      }

    },
    {
      'selector': 'div.inner',
      'properties': {
        'x': {
          'background-position-x': {
            'initial': 0,
            'multiplier': 0.2
          }
        }
      }
    }
  ]

});

function addProduto(e, $ID) {
  var $select = '<div class="selecionado">' +
          '<input type="button" onclick="removerProduto(this, ' + $ID + ');" class="btn add remove ir" title="Remover da lista de desejo" />' +
          '<span>Produto Selecionado</span>' +
          '</div>';
  $(e).parents('.categoria').prepend($select);
  ativador();
}
function removerProduto(e, $ID) {
  $(e).parent('.selecionado').remove();
}